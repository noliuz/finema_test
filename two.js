function check(expr){
  const holder = []
  const openBrackets = ['(','{','[']
  const closedBrackets = [')','}',']']

  //check leter in order
  for (let letter of expr) {
    //if open_type add it to holder
    if (openBrackets.includes(letter)) {
      holder.push(letter)
    } else if (closedBrackets.includes(letter)) {
      // if next letter is close_type then check that last in holder is its pair
        //find open with same type
      const openPair = openBrackets[closedBrackets.indexOf(letter)]
        //if found same pair then remove open_type at last index in holder
      if (holder[holder.length - 1] === openPair) {
        holder.splice(-1,1)
      } else {  // if not same pair , this expr is false
        return false
      }
    }
  }
  //there is not wrong pairs return true
  return true
}

var fs = require('fs');
fs.readFile('2nd_exam_input_test.txt',(err,data) => {
  //console.log(data.toString())
  let dataArr = data.toString().split('\n')

  //check if last line is whitespace
  if (dataArr[dataArr.length-1] == '')
    dataArr.splice(-1,1)

  let dataTestLength = dataArr[0]
  dataArr.splice(0,1)
  let dataTestArr = dataArr;

  console.log(dataTestLength,dataTestArr.length)

  //check input data correction
  if (dataTestLength != dataTestArr.length) {
    console.log('Data length is not equal count of data line.')
    process.exit(1)
  }

  //start print report
  dataTestArr.map((val,idx,arr) => {
    //check q string length
    if (val.length > 100000) {
      console.log('Data length is not equal count of data line.')
      process.exit(2)
    }

    if (check(val))
      console.log("yes")
    else {
      console.log("no")
    }
  })
})
