function checkCountOfDifferentCharacter(str1,str2) {
  let diffCount = 0
  for (let i=0;i<str1.length;i++) {
    if (str1[i] != str2[i])
      diffCount++
  }

  return diffCount
}

var fs = require('fs');
fs.readFile('3rd_exam_input_test.txt',(err,data) => {
  let dataArr = data.toString().split("\n")
  //check if last line is whitespace
  if (dataArr[dataArr.length-1] == '')
    dataArr.splice(-1,1)

  //check that word size is between 3 - 1000
  if (!(dataArr[0] >= 3 && dataArr[0] <= 1000)) {
    console.log('Word size must between 3 to 1000')
    process.exit(3)
  }

  //check line count must between 1 to 30000
  if (!(dataArr[1] >= 1 && dataArr[0] <= 30000)) {
    console.log('Word count must between 1 to 30000')
    process.exit(3)
  }


  //check size of word
  let firstLineSize = dataArr[0];
  let wordSize = dataArr[2].trim().length;
  if (firstLineSize != wordSize) {
    console.log('Word size is wrong.')
    process.exit(1)
  }

  //check word count
  let secondLineWordCount = dataArr[1];
  let wordCount = dataArr.length-2
  if (secondLineWordCount != wordCount) {
    console.log('Wrong word count')
    process.exit(2)
  }

  //check if there is only 1 word
  if (wordCount == 1) {
    console.log(dataArr[2])
    process.exit(0)
  }



  //remove whitespace
  for (let i=0;i<dataArr.length;i++) {
    dataArr[i] = dataArr[i].trim()
  }

  //remove number
  dataArr.splice(0,2)

  //start to find output
  for (let i=1;i<dataArr.length;i++) {
    let oneWord = dataArr[i-1]
    let twoWord = dataArr[i]

    if (checkCountOfDifferentCharacter(oneWord,twoWord) > 2) {
      console.log(oneWord)
      process.exit(0)
    }
  }

  //console.log(dataArr)
})
