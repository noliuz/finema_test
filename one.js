//read file
var fs = require('fs');
fs.readFile('1st_exam_input_test.txt',(err,data) => {
  let dataArr = data.toString().split("\n")

  //check if last line is whitespace
  if (dataArr[dataArr.length-1] == '')
    dataArr.splice(-1,1)

  let numberCount = dataArr[0].trim()
  let inputNumberString = dataArr[1].trim()
  let inputNumberArr = inputNumberString.split(' ')

  //check if first line is between 2 to 10
  if (!(numberCount >= 2 && numberCount <= 10)) {
    console.log('Number at first line is not in range.')
    process.exit(2)
  }

  //check number count
  if (numberCount != inputNumberArr.length) {
    console.log('Wrong number count at first line.')
    process.exit(1)
  }

  //remove repeated number
  let noRepeatNumberArr = []
  inputNumberArr.map((val) => {
    if (noRepeatNumberArr.indexOf(val) == -1)
      noRepeatNumberArr.push(val)
  })

  //sum all number
  let sumAllNumberResult = 0
  noRepeatNumberArr.map((val) =>{
    sumAllNumberResult += parseInt(val)
  })

  console.log(sumAllNumberResult)
})
